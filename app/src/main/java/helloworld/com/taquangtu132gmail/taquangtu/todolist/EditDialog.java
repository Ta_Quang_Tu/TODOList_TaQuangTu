package helloworld.com.taquangtu132gmail.taquangtu.todolist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditDialog extends AppCompatActivity {

    private Button btUpdate, btCancel;
    private EditText etTaskContent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_dialog);
        map();
        setOnClickView();
    }
    private void map()
    {
        btCancel=(Button) findViewById(R.id.btCancel);
        btUpdate= (Button) findViewById(R.id.btUpdate);
        etTaskContent =(EditText) findViewById(R.id.etTaskContent);
    }
    void setOnClickView()
    {
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditDialog.this.finish();   //focus this
            }
        });
        btUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String editedString = etTaskContent.getText().toString().trim();
                if(editedString=="")
                {
                    Toast.makeText(EditDialog.this,"Task content has not been typed, try again!!!",Toast.LENGTH_SHORT).show();
                }
                else
                    {
                        // EditDialog.this.getApplicationContext();
                    }
            }
        });
    }
}
