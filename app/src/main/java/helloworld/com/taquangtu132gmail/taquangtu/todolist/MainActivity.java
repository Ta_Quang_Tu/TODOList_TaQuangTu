package helloworld.com.taquangtu132gmail.taquangtu.todolist;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Database toDoDatabase;
    private Button btAdd;
    private EditText etTaskContent;
    private ListView lvTaskList;
    private ArrayList<String> taskArray;
    private TaskAdapter taskAdapter;
    public Database getDataBase()
    {
        return this.toDoDatabase;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mapView();
        toDoDatabase =new Database(this,"todoDatabase.sqlite",null,1);
        this.taskArray=new ArrayList<>();
        this.taskAdapter=new TaskAdapter(this,R.layout.activity_task_row,taskArray);
        lvTaskList.setAdapter(taskAdapter);
        toDoDatabase.doSQL("Create Table If not exists task(ID Integer Primary Key Autoincrement , taskContent Varchar(100))");

       // demo insert
        Cursor cursor = toDoDatabase.getData("SELECT * FROM task");
        while (cursor.moveToNext())
        {
           taskArray.add(cursor.getString(1).toString().trim());
        }
        taskAdapter.notifyDataSetChanged();
        setOnClickListener();
    }
    void mapView()
    {
        this.btAdd         = (Button)findViewById(R.id.btAdd);
        this.etTaskContent = (EditText) findViewById(R.id.etTaskContent);
        this.lvTaskList    = (ListView) findViewById(R.id.lvTaskList);
    }
    void setOnClickListener()
    {
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String taskContent = etTaskContent.getText().toString().trim();
                if(taskContent.isEmpty())
                {
                    Toast.makeText(MainActivity.this, "Công việc chưa được nhập", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    taskArray.add(taskContent);
                    toDoDatabase.doSQL("INSERT INTO task values(null,'" + taskContent + "')");
                    taskAdapter.notifyDataSetChanged();
                }
                etTaskContent.setText("");
            }
        });
    }
}
