package helloworld.com.taquangtu132gmail.taquangtu.todolist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class TaskRowActivity extends AppCompatActivity {

    private ImageButton imbDelete, imbEdit;
    private TextView tvTaskContent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_row);
        mapView();
        setOnClickListener();
    }
    private void mapView()
    {
        imbDelete = (ImageButton) findViewById(R.id.imbDelete);
        imbEdit   = (ImageButton) findViewById(R.id.imbEdit);
    }
    private void setOnClickListener()
    {
        imbEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(TaskRowActivity.this, "Sửa", Toast.LENGTH_SHORT).show();
                tvTaskContent.setText("Đã sửa");
            }
        });
        imbDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Toast.makeText(TaskRowActivity.this, "Xóa", Toast.LENGTH_SHORT).show();
                    tvTaskContent.setText("Đã xóa");
            }
        });
    }
}
