package helloworld.com.taquangtu132gmail.taquangtu.todolist;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import static helloworld.com.taquangtu132gmail.taquangtu.todolist.R.id.imbEdit;

/**
 * Created by Ta Quang Tu on 19/04/2018.
 */
class ViewHolder
{
     TextView tvTaskContent;
     ImageButton imbDelete;
     ImageButton imbEdit;
};
public class TaskAdapter extends BaseAdapter {
    private MainActivity context;
    private int layoutResource;
    private List<String> taskList;
    public TaskAdapter(MainActivity context, int layoutResource, List<String> taskList)
    {
        this.context=context;
        this.layoutResource=layoutResource;
        this.taskList=taskList;
    }
    @Override
    public int getCount() {
        return taskList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if(convertView==null)
        {
            viewHolder=new ViewHolder();
            LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_task_row,null);
            viewHolder.tvTaskContent =(TextView) convertView.findViewById(R.id.tvTaskContent);
            viewHolder.imbEdit=(ImageButton) convertView.findViewById(imbEdit);
            viewHolder.imbDelete=(ImageButton) convertView.findViewById(R.id.imbDelete);
            convertView.setTag(viewHolder);
        }
        else
            {
                viewHolder = (ViewHolder) convertView.getTag();
            }
        viewHolder.imbDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder al =new AlertDialog.Builder(context);
                al.setMessage("Bạn có thật sự muốn xóa?");
                al.setTitle("Chú ý!!!");
                al.setCancelable(true);
                al.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing here
                    }
                });
                al.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String deletedString=taskList.get(position).toString().trim();
                        taskList.remove(position);
                        TaskAdapter.this.notifyDataSetChanged();
                        Database db= context.getDataBase();
                        db.doSQL("Delete from task where taskContent = '"+ deletedString +"'");
                    }
                });
                al.show();
            }
        });
        viewHolder.imbEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.activity_edit_dialog);
                dialog.setCancelable(true);
                dialog.setTitle("Edit");
                // map
                final EditText editedText;
                editedText = (EditText) dialog.findViewById(R.id.etTaskContent);
                Button btCancel     = (Button) dialog.findViewById(R.id.btCancel);
                Button btUpdate     = (Button) dialog.findViewById(R.id.btUpdate);
                final String oldTaskContent = viewHolder.tvTaskContent.getText().toString().trim();
                editedText.setText(oldTaskContent);
                //set on click before dialog showing
                btCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                btUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String editedString = editedText.getText().toString().trim();
                        if (editedString.equals("")) {
                            Toast.makeText(dialog.getContext(),"Task content has not been typed, try again!!!", Toast.LENGTH_SHORT).show();
                        }
                        else
                            {
                                viewHolder.tvTaskContent.setText(editedString);
                                Database db = context.getDataBase();
                                db.doSQL("update task set taskContent ='" + editedString + "' where taskContent = '"+ oldTaskContent +"'");
                                taskList.set(position, editedString);
                                TaskAdapter.this.notifyDataSetChanged();
                                Toast.makeText(context,"Updating successfully", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                    }
                });
                //
                dialog.show();
            }
        });
        viewHolder.tvTaskContent.setText(taskList.get(position));
        viewHolder.imbDelete.setImageResource(R.drawable.cancel);
        viewHolder.imbEdit.setImageResource(R.drawable.edit);
        return convertView;
    }
}
