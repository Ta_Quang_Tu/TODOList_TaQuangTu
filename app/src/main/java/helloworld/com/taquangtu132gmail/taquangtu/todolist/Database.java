package helloworld.com.taquangtu132gmail.taquangtu.todolist;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Ta Quang Tu on 19/04/2018.
 */

public class Database extends SQLiteOpenHelper
{
    public Database(Context context, String dbName, SQLiteDatabase.CursorFactory cursorFactory, int version)
    {
        super(context,dbName,cursorFactory,version);
    }
    //query without return cursor
    public void doSQL(String sql)
    {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL(sql);
    }
    //query and get return cursor
    public Cursor getData(String sql)
    {
        SQLiteDatabase database = getWritableDatabase();
        return database.rawQuery(sql,null);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
